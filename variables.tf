# Ce fichier définit les variables d'entrée pour la configuration Terraform.
# Il permet de personnaliser le comportement des modules et des ressources.

# Définition des variables locales pour une meilleure réutilisabilité
locals {
  aws_region  = "eu-west-1"
  bucket_name = "my-static-website-bucket"
}

variable "aws_region" {
  description = "The AWS region to deploy the infrastructure in."
  default     = local.aws_region
}
