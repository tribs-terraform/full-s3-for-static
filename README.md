# Projet full-s3-for-static

## Objectif

Ce projet Terraform est conçu pour déployer une infrastructure AWS pour héberger un site statique en HTML sur du stockage S3 et avec CloudFront. Il offre une flexibilité pour gérer les certificats SSL, les domaines et l'authentification AWS, et permet une réécriture d'URL via une fonction Lambda.

## Modules

- **s3**: Crée un bucket S3 pour le stockage du site statique.
- **cloudfront**: Configure une distribution CloudFront pour le CDN.
- **letsencrypt**: Gère les certificats SSL LetsEncrypt.
- **route53**: Gère les domaines avec AWS Route53.
- **iam_secrets_manager**: Gère les clés d'accès AWS avec Secrets Manager.
- **lambda**: Gère la réécriture d'URL via une fonction Lambda.

## Utilisation locale

1. Configurez vos clés d'accès AWS et autres variables sensibles dans un fichier `terraform.tfvars` à la racine du projet.
2. Placez les fichiers du site dans le répertoire `www/`.
3. Exécutez `terraform init` pour initialiser le projet.
4. Exécutez `terraform apply` pour déployer l'infrastructure.

## Utilisation via CI/CD

1. Configurez les variables CI/CD du projet GitLab avec vos clés d'accès AWS et autres variables sensibles.
2. La pipeline CI/CD se chargera de déployer l'infrastructure à chaque push.

## Variables

- **aws_access_key**: Clé d'accès AWS.
- **aws_secret_key**: Clé secrète AWS.
- **domain_name**: Nom de domaine à utiliser.
- ... (autres variables spécifiques à chaque module)

## Outputs

- **s3_bucket_name**: Nom du bucket S3 créé.
- **cloudfront_distribution_id**: ID de la distribution CloudFront.
- ... (autres outputs spécifiques à chaque module)

## Environnements

Les configurations spécifiques à chaque environnement (dev, prod) se trouvent dans le répertoire `environments/`. Chaque environnement a ses propres fichiers `main.tf`, `variables.tf` et `outputs.tf`.

