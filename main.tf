# Ce fichier est le point d'entrée principal pour la configuration Terraform.
# Il initialise les modules et les ressources nécessaires pour le déploiement de l'infrastructure.

# Configuration du fournisseur AWS
# Cette section configure le fournisseur AWS avec la région spécifiée.
provider "aws" {
  region = local.aws_region  # Utilisation de la variable locale pour la région
}

# Appel du module S3 pour la création du bucket
module "s3" {
  source = "./modules/s3"
  bucket_name = local.bucket_name  # Utilisation de la variable locale pour le nom du bucket
}

# Appel du module CloudFront pour la distribution CDN
module "cloudfront" {
  source = "./modules/cloudfront"
}

# Appel du module LetsEncrypt pour la gestion des certificats SSL
module "letsencrypt" {
  source = "./modules/letsencrypt"
}

# Appel du module Route53 pour la gestion des domaines
module "route53" {
  source = "./modules/route53"
}

# Appel du module IAM Secrets Manager pour la gestion des clés d'accès
module "iam_secrets_manager" {
  source = "./modules/iam_secrets_manager"
}

# Appel du module Lambda pour la réécriture d'URL
module "lambda" {
  source = "./modules/lambda"
}
