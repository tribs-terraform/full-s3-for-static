# Sortie pour l'ID du VPC
# Cette sortie est utile pour référencer le VPC dans d'autres modules ou projets.
output "vpc_id" {
  description = "L'ID du VPC créé."
  value       = module.networking.vpc_id
}

# Sortie pour l'ID de l'instance EC2
# Cette sortie permet de récupérer l'ID de l'instance EC2 pour des utilisations ultérieures.
output "ec2_instance_id" {
  description = "L'ID de l'instance EC2 créée."
  value       = module.ec2_instance.instance_id
}

# Sortie pour l'ARN de la fonction Lambda
# Cette sortie est utile pour intégrer la fonction Lambda dans d'autres services AWS.
output "lambda_function_arn" {
  description = "L'ARN de la fonction Lambda créée."
  value       = module.lambda_url_rewrite.lambda_arn
}

# Sortie pour le point de terminaison de l'instance RDS
# Cette sortie permet de récupérer l'URL de l'instance RDS pour la connexion à la base de données.
output "rds_instance_endpoint" {
  description = "Le point de terminaison de l'instance RDS créée."
  value       = module.rds_instance.rds_endpoint
}

# Sortie pour le nom du bucket S3
# Cette sortie est utile pour référencer le bucket S3 dans d'autres modules ou projets.
output "s3_bucket_name" {
  description = "Le nom du bucket S3 créé."
  value       = module.s3_bucket.bucket_name
}

# Sortie pour l'ID de la distribution CloudFront
# Cette sortie permet de récupérer l'ID de la distribution pour des utilisations ultérieures.
output "cloudfront_distribution_id" {
  description = "L'ID de la distribution CloudFront créée."
  value       = module.cloudfront.distribution_id
}

# Sortie pour l'ARN du certificat Let's Encrypt
# Cette sortie est utile pour intégrer le certificat SSL dans d'autres services AWS.
output "letsencrypt_cert_arn" {
  description = "L'ARN du certificat Let's Encrypt créé."
  value       = module.letsencrypt.cert_arn
}

# Sortie pour l'ID de la zone Route53
# Cette sortie permet de récupérer l'ID de la zone pour des utilisations ultérieures.
output "route53_zone_id" {
  description = "L'ID de la zone Route53 créée."
  value       = module.route53.zone_id
}

# Sortie pour l'ARN du gestionnaire de secrets IAM
# Cette sortie est utile pour référencer le secret dans d'autres modules ou projets.
output "iam_secrets_manager_arn" {
  description = "L'ARN du gestionnaire de secrets IAM créé."
  value       = module.iam_secrets_manager.secret_arn
}
