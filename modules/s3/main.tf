# Création d'un bucket S3 pour héberger un site web statique
resource "aws_s3_bucket" "static_website" {
  bucket = "my-static-website-bucket" # Nom du bucket
  acl    = "public-read"               # Accès en lecture publique

  # Configuration du site web statique
  website {
    index_document = "index.html"  # Document d'index
    error_document = "error.html"  # Document d'erreur
  }
}

# Politique du bucket pour permettre un accès en lecture publique
resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.static_website.bucket # Référence au bucket créé précédemment

  # Politique JSON pour autoriser l'accès en lecture publique
  policy = jsonencode({
    Version   = "2012-10-17",
    Statement = [
      {
        Sid       = "PublicReadGetObject",
        Effect    = "Allow",
        Principal = "*",
        Action    = ["s3:GetObject"],
        Resource  = [aws_s3_bucket.static_website.arn]
      }
    ]
  })
}
