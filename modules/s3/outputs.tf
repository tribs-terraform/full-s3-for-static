# Sortie pour l'ARN du bucket S3
output "bucket_arn" {
  description = "The ARN of the S3 bucket"  # Description de la sortie
  value       = aws_s3_bucket.static_website.arn  # Valeur de la sortie
}
