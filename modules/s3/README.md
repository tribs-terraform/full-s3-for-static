# S3 Module

This module provisions an S3 bucket for static website hosting.

## Variables:

- `bucket_name`: The name of the S3 bucket. Default is "my-static-website-bucket".

## Resources Provisioned:

- An S3 bucket for static website hosting.
- An S3 bucket policy to allow public read access.

## Outputs:

- `bucket_arn`: The ARN of the S3 bucket.
