# Variable pour le nom du bucket S3
variable "bucket_name" {
  description = "The name of the S3 bucket"  # Description de la variable
  default     = local.bucket_name  # Utilisation de la variable locale comme valeur par défaut
}
