# Variable pour le nom de la distribution CloudFront
variable "distribution_name" {
  description = "The name of the CloudFront distribution" # Description de la variable
  default     = "s3-website-cloudfront-distribution"     # Valeur par défaut
}
