# Sortie pour l'ID de la distribution CloudFront
output "distribution_id" {
  description = "The ID of the CloudFront distribution" # Description de la sortie
  value       = aws_cloudfront_distribution.s3_distribution.id # Valeur de la sortie
}
