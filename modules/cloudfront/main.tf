# Configuration de la distribution CloudFront pour le site web statique
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.static_website.bucket_regional_domain_name # Utilisation du domaine régional du bucket S3
    origin_id   = "S3Origin" # ID unique pour l'origine

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path # Configuration de l'identité d'accès
    }
  }

  # Configuration de l'adresse par défaut pour la distribution
  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"] # Méthodes HTTP autorisées
    cached_methods   = ["GET", "HEAD"] # Méthodes HTTP mises en cache
    target_origin_id = "S3Origin" # Référence à l'ID d'origine

    forwarded_values {
      query_string = false # Ne pas transmettre la chaîne de requête

      cookies {
        forward = "none" # Ne pas transmettre les cookies
      }
    }

    viewer_protocol_policy = "redirect-to-https" # Rediriger le trafic HTTP vers HTTPS
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # Configuration de la zone de disponibilité et des performances
  price_class = "PriceClass_100" # Utilisation de la classe de prix standard

  # Configuration du certificat SSL/TLS
  viewer_certificate {
    cloudfront_default_certificate = true # Utilisation du certificat par défaut de CloudFront
  }

  restrictions {
    geo_restriction {
      restriction_type = "none" # Pas de restrictions géographiques
    }
  }

  # Configuration de l'invalidation du cache
  default_root_object = "index.html" # Document racine par défaut

  tags = {
    Name        = "s3-website-cloudfront-distribution"
    Environment = "dev"
  }
}

# Configuration de l'identité d'accès pour CloudFront
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "Access identity for the S3 static website" # Commentaire pour l'identité d'accès
}
