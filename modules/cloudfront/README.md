# Module CloudFront

Ce module Terraform est conçu pour créer une distribution CloudFront pour un site web statique hébergé dans un bucket S3.

## Variables

- `distribution_name`: Le nom de la distribution CloudFront. Par défaut, il est défini sur "s3-website-cloudfront-distribution".

## Ressources provisionnées

1. **aws_cloudfront_distribution.s3_distribution**: Cette ressource crée une distribution CloudFront pour le site web statique. Elle est configurée pour utiliser le domaine régional du bucket S3 comme origine et pour rediriger tout le trafic HTTP vers HTTPS.

2. **aws_cloudfront_origin_access_identity.origin_access_identity**: Cette ressource crée une identité d'accès pour CloudFront, permettant à CloudFront d'accéder au contenu du bucket S3.

## Outputs

- `distribution_id`: L'ID de la distribution CloudFront créée.

## Utilisation

Pour utiliser ce module dans votre code Terraform, ajoutez le code suivant à votre fichier:

```hcl
module "cloudfront" {
  source = "./modules/cloudfront"
}
