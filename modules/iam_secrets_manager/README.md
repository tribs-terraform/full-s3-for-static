# Module IAM & Secrets Manager

Ce module Terraform est conçu pour gérer l'authentification AWS, la création des clés IAM, et la rotation des clés avec AWS Secrets Manager et AWS Lambda.

## Ressources provisionnées

1. **aws_iam_policy.secrets_manager_access**: Politique IAM pour permettre l'accès à Secrets Manager.
2. **aws_iam_role.lambda_secrets_manager_role**: Rôle IAM pour Lambda pour accéder à Secrets Manager.
3. **aws_secretsmanager_secret.iam_keys**: Secret dans Secrets Manager pour stocker les clés IAM.

## Utilisation

Pour utiliser ce module dans votre code Terraform, ajoutez le code suivant à votre fichier:

```hcl
module "iam_secrets_manager" {
  source = "./modules/iam_secrets_manager"
}
