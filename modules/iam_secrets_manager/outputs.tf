# ARN du secret dans Secrets Manager
output "secret_arn" {
  value = aws_secretsmanager_secret.iam_keys.arn
}
