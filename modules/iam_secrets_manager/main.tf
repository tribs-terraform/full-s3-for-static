# Création d'une politique IAM pour permettre l'accès à Secrets Manager
resource "aws_iam_policy" "secrets_manager_access" {
  name        = "SecretsManagerFullAccess"
  description = "Accès complet à AWS Secrets Manager"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action   = "secretsmanager:*",
        Resource = "*",
        Effect   = "Allow"
      }
    ]
  })
}

# Création d'un rôle IAM pour Lambda pour accéder à Secrets Manager
resource "aws_iam_role" "lambda_secrets_manager_role" {
  name = "LambdaSecretsManagerRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
        Effect = "Allow",
        Sid    = ""
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_secrets_manager_attach" {
  policy_arn = aws_iam_policy.secrets_manager_access.arn
  role       = aws_iam_role.lambda_secrets_manager_role.name
}

# Création d'un secret dans Secrets Manager pour stocker les clés IAM
resource "aws_secretsmanager_secret" "iam_keys" {
  name = "IAM_Keys"
}

resource "aws_secretsmanager_secret_version" "iam_keys_version" {
  secret_id     = aws_secretsmanager_secret.iam_keys.id
  secret_string = "{\"access_key\": \"YOUR_ACCESS_KEY\", \"secret_key\": \"YOUR_SECRET_KEY\"}"
}
