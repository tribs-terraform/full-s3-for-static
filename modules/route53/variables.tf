# Le nom de domaine pour lequel la zone DNS sera créée
variable "domain_name" {
  description = "Le nom de domaine pour lequel la zone DNS sera créée"
}

# L'adresse IP du site web
variable "website_ip" {
  description = "L'adresse IP du site web"
}
