# Module Route53

Ce module Terraform est conçu pour créer une zone DNS pour un domaine spécifique et un enregistrement DNS pour le site web.

## Variables

- `domain_name`: Le nom de domaine pour lequel la zone DNS sera créée.
- `website_ip`: L'adresse IP du site web.

## Ressources provisionnées

1. **aws_route53_zone.main**: Crée une zone DNS pour le domaine.
2. **aws_route53_record.website**: Crée un enregistrement DNS pour le site web.

## Outputs

- `zone_id`: L'ID de la zone DNS créée.

## Utilisation

Pour utiliser ce module dans votre code Terraform, ajoutez le code suivant à votre fichier:

```hcl
module "route53" {
  source      = "./modules/route53"
  domain_name = "votre_domaine.com"
  website_ip  = "votre_ip"
}
