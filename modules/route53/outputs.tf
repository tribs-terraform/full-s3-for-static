# L'ID de la zone DNS créée
output "zone_id" {
  value = aws_route53_zone.main.id
}
