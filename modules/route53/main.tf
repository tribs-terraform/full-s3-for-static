# Création d'une zone DNS pour le domaine
resource "aws_route53_zone" "main" {
  name = var.domain_name
}

# Création d'un enregistrement DNS pour le site web
resource "aws_route53_record" "website" {
  zone_id = aws_route53_zone.main.id
  name    = var.domain_name
  type    = "A"
  ttl     = "300"
  records = [var.website_ip]
}
