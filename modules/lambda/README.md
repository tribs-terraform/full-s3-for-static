# Module Lambda

Ce module Terraform est conçu pour gérer la réécriture d'URL et le traitement des fichiers HTML à l'aide d'une fonction AWS Lambda.

## Ressources provisionnées

1. **aws_lambda_function.url_rewrite**: Fonction Lambda pour la réécriture d'URL.
2. **aws_lambda_permission.allow_cloudfront**: Permissions pour permettre à CloudFront d'appeler la fonction Lambda.

## Utilisation

Pour utiliser ce module dans votre code Terraform, ajoutez le code suivant à votre fichier:

```hcl
module "lambda" {
  source = "./modules/lambda"
}
