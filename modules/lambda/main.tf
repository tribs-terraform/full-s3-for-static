# Création d'une fonction Lambda pour la réécriture d'URL
resource "aws_lambda_function" "url_rewrite" {
  filename      = "lambdaRewrite.zip"
  function_name = "urlRewrite"
  role          = aws_iam_role.lambda_secrets_manager_role.arn
  handler       = "lambdaRewrite.handler"
  runtime       = "nodejs14.x"
  source_code_hash = filebase64sha256("lambdaRewrite.zip")
}

# Permissions pour permettre à CloudFront d'appeler la fonction Lambda
resource "aws_lambda_permission" "allow_cloudfront" {
  statement_id  = "AllowExecutionFromCloudFront"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.url_rewrite.function_name
  principal     = "edgelambda.amazonaws.com"
}
