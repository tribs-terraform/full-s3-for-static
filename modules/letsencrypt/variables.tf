# L'ID de la zone Route53 pour le domaine
variable "zone_id" {
  description = "L'ID de la zone Route53 pour le domaine"
}

# Le nom de domaine pour lequel le certificat SSL sera généré
variable "domain_name" {
  description = "Le nom de domaine pour lequel le certificat SSL sera généré"
}

# Le domaine de validation pour Let's Encrypt
variable "validation_domain" {
  description = "Le domaine de validation pour Let's Encrypt"
}
