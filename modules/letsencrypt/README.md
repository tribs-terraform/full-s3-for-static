# Module Let's Encrypt

Ce module Terraform est conçu pour générer un certificat SSL pour un domaine spécifique en utilisant Let's Encrypt.

## Variables

- `zone_id`: L'ID de la zone Route53 pour le domaine.
- `domain_name`: Le nom de domaine pour lequel le certificat SSL sera généré.
- `validation_domain`: Le domaine de validation pour Let's Encrypt.

## Ressources provisionnées

1. **aws_route53_record.letsencrypt_validation**: Crée un enregistrement DNS pour valider le certificat SSL avec Let's Encrypt.
2. **aws_acm_certificate.cert**: Crée le certificat SSL avec Let's Encrypt.

## Outputs

- `certificate_arn`: L'ARN du certificat SSL généré.

## Utilisation

Pour utiliser ce module dans votre code Terraform, ajoutez le code suivant à votre fichier:

```hcl
module "letsencrypt" {
  source         = "./modules/letsencrypt"
  zone_id        = "votre_zone_id"
  domain_name    = "votre_domaine.com"
  validation_domain = "votre_domaine_validation.com"
}
