# Création d'un enregistrement DNS pour valider le certificat SSL avec Let's Encrypt
resource "aws_route53_record" "letsencrypt_validation" {
  zone_id = var.zone_id
  name    = var.domain_name
  type    = "CNAME"
  ttl     = "300"
  records = [var.validation_domain]
}

# Création du certificat SSL avec Let's Encrypt
resource "aws_acm_certificate" "cert" {
  domain_name       = var.domain_name
  validation_method = "DNS"
}
