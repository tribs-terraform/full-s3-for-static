# L'ARN du certificat SSL généré
output "certificate_arn" {
  value = aws_acm_certificate.cert.arn
}
